﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class RegisterCardAsActive : MonoBehaviour, ITrackableEventHandler
{
    public CurrentActivesCards currentActivesCards;
    static public bool stopTrackEvent = false;

    private TrackableBehaviour mTrackableBehaviour;
    void Start()
    {

        currentActivesCards.RegisterInAllCards(this.gameObject);

        mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (mTrackableBehaviour)
        {
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }
    }

    public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
    {
        if (stopTrackEvent)
            return;

        if (newStatus == TrackableBehaviour.Status.DETECTED ||
           newStatus == TrackableBehaviour.Status.TRACKED ||
           newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {

            currentActivesCards.AddToList(this.gameObject);
        }
        else
        {


            currentActivesCards.RemoveFromList(this.gameObject);
        }
    }
}
