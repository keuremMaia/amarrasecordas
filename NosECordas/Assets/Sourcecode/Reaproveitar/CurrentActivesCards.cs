﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

[CreateAssetMenu(fileName = "CurrentActiveCards", menuName = "Vinicius/CurrentActiveCards", order = 0)]
public class CurrentActivesCards : ScriptableObject
{
    public List<GameObject> activesCards = new List<GameObject>();
    public List<GameObject> AllCards = new List<GameObject>();

    public void AddToList(GameObject card)
    {

        activesCards.Add(card);
    }


    public void RegisterInAllCards(GameObject Card)
    {
        AllCards.Add(Card);
    }
    public void RemoveFromList(GameObject card)
    {
        activesCards.Remove(card);
    }

    public void Lock()
    {
        foreach (GameObject card in activesCards)
        {
            OnTargetFoundVuforia.stopTrackEvent = true;
            RegisterCardAsActive.stopTrackEvent = true;
            card.GetComponent<ImageTargetBehaviour>().enabled = false;
        }


    }

    public void UnLock()
    {
        for (int i = 0; i < activesCards.Count; i++)
        {
            OnTargetFoundVuforia.stopTrackEvent = false;
            RegisterCardAsActive.stopTrackEvent = false;
            activesCards[i].GetComponent<ImageTargetBehaviour>().enabled = true;
            activesCards[i].GetComponent<OnTargetFoundVuforia>().OnExitUnlock();
        }


    }

    public void Sound()
    {
        AudioListener.pause = false;
    }

    public void Mute()
    {
        AudioListener.pause = true;
    }

}
