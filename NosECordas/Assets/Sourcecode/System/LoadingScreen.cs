﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace SoureCode.System
{
    public class LoadingScreen : MonoBehaviour
    {

        public int sceneIndex;


        private void Start()
        {
            StartCoroutine(LoadAsynchrously(sceneIndex));
        }

        IEnumerator LoadAsynchrously(int sceneIndex)
        {

            yield return new WaitForSeconds(0);
            AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);


        }
    }
}
