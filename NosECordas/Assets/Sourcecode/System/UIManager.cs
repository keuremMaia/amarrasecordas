﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace SoureCode.System
{
    public class UIManager : MonoBehaviour
    {
        [Header("Buttons")]
        public Button lockOnObj;
        public Button CloseAppButton;
        public Button InformationButton;
        public Button LupaButton;
        public Button backButton;

        [Header("Buttons Images")]
        public Image InfoImage;
        public Sprite LockOnImage;
        public Sprite LockOffImage;

        [Header("Info Card")]
        public Animator infoAnim;
        public Button infoCard;
        public string OpenAnim = "Open Info";
        public string CloseAnim = "Close Info";
        public bool isOpen = false;

        SystemManager systemManager;

        public void Init(SystemManager sm)
        {
            systemManager = sm;
        }

        public void UITick()
        {
            switch (systemManager.typeDetected)
            {
                case CardTypeDetected.None:
                    systemManager.ResetAll();
                    break;
                case CardTypeDetected.Alive:
                    systemManager.RotateObject();
                    break;
                case CardTypeDetected.Comparacoes:

                    break;
                default:
                    break;
            }
        }

        #region InfoCard
        public void InfoCardExecution()
        {
            if (systemManager.typeDetected == CardTypeDetected.None) { return; }

            if (isOpen)
            {
                infoCard.onClick.RemoveAllListeners();
                infoCard.onClick.AddListener(delegate { CloseInfoCard(CloseAnim); });
            }
            else
            {
                infoCard.onClick.RemoveAllListeners();
                infoCard.onClick.AddListener(delegate { OpenInfoCard(OpenAnim); });
            }
        }

        public void CloseInfoCard(string targetAnim)
        {
            if (systemManager.typeDetected == CardTypeDetected.None) { return; }

            infoAnim.Play(targetAnim);
            isOpen = false;
        }

        public void OpenInfoCard(string targetAnim)
        {
            if (systemManager.typeDetected == CardTypeDetected.None) { return; }

            infoAnim.Play(targetAnim);
            isOpen = true;
        }
        #endregion


    }
}
