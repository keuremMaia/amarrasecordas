﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SoureCode.System
{
    public class SliderAnimControl : MonoBehaviour
    {  
        public Slider slider;
        public string animName = "Scene";
        //bool invert = false;
        
        public ParticleSystem vapor;
        public ParticleSystem chuva;
        public ParticleSystem nuvens;
        public SkinnedMeshRenderer sk;
        public float maxSpeed = 0.1f;
        Animator anim;

        private float lastValue;
        // Use this for initialization
        void Start()
        {
            anim = GetComponent<Animator>();
            anim.speed = 0;
            anim.Play(animName, 0, 0);
        }

        public void OnValueChanged(float changedValue)
        {
            if (changedValue - lastValue > maxSpeed)
            {
                changedValue = lastValue + maxSpeed;
                slider.value = lastValue + maxSpeed;
            }else if (lastValue-changedValue > maxSpeed)
            {
                changedValue = lastValue - maxSpeed;
                slider.value = lastValue - maxSpeed;
            }

            anim.speed = 0.0001f;
            anim.Play(animName, 0, (changedValue * 2));

            if (slider.value < 0.5f)
            {
                sk.enabled = true;
                ParticleSystem.EmissionModule emiVapor = vapor.emission;
                emiVapor.enabled = false;
                vapor.Stop(false, ParticleSystemStopBehavior.StopEmittingAndClear);

                ParticleSystem.EmissionModule emiNuvem = nuvens.emission;
                emiNuvem.enabled = false;
                nuvens.Stop(false,ParticleSystemStopBehavior.StopEmittingAndClear);
            }


            if (slider.value > .8f)
            {
                ParticleSystem.EmissionModule emiVapor = vapor.emission;
                emiVapor.enabled = true;
                vapor.Play();
            }
            if (slider.value == 1)
            {
                sk.enabled = false;
                ParticleSystem.EmissionModule emiNuvem = nuvens.emission;
                emiNuvem.enabled = true;
                nuvens.Play();

                ParticleSystem.EmissionModule emiVapor = vapor.emission;
                emiVapor.enabled = false;
                vapor.Stop();
            }

            lastValue = changedValue;
        }
    }
}
