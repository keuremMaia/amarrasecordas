﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace SoureCode.System
{
    public class ObjectInitialized : MonoBehaviour
    {
        [Header("Plataforma")]
        public Transform plataformY;
        public Transform Root;

        [Header("Imager")]
        public Sprite Information;

        [Header("Execution")]
        public Animator anim;
        public string targetAnim;
        public CardTypeDetected cardType;
        public OnTargetFoundVuforia vufo;
        public bool isAlive = false;
        public bool playOnAlive = false;
        public bool hasAnim = false;

        [Header("N Executar")]
        public bool dntExe;

        private new Renderer renderer;

        private void Start()
        {
            if (dntExe) { return; }

            renderer = GetComponentInChildren<Renderer>();
        }

        public float rotSpeed = 20f;

        private void Update()
        {
            if (dntExe) { return; }

            isAlive = renderer.enabled;

            if (isAlive)
            {
                SystemManager.instace.plataform = plataformY;
                SystemManager.instace.typeDetected = cardType;
                SystemManager.instace.plataformRoot = Root;
                SystemManager.instace.vufo = vufo;

                if (Information != null)
                {
                    SystemManager.instace.uIManager.InfoImage.sprite = Information;
                }

                if (hasAnim)
                {
                    if (anim != null)
                    {
                        anim.Play(targetAnim);
                    }
                }

                SystemManager.instace.uIManager.InfoCardExecution();
            }
        }
    }
}
