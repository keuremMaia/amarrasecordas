﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//A placa vai piscar as partes da cadeia e a criança seleciona os alimentos correspondentes. (Tipo do corpo humano)

namespace SoureCode.System
{
    public class PiramideGame : MonoBehaviour
    {
        public GameObject gObj = null;
        public Transform anchorHolder = null;
        public Text points;
        public int point = 0;

        private Plane objPlane;
        private Vector3 m0;
        public bool initGame = false;
        public bool placeOn;
        public Collider PlaceHolder;
        public TriggerDetection td;

        public List<Transform> Anchors = new List<Transform>();
        public List<Transform> PlaceHolders = new List<Transform>();
        public List<Transform> Objs = new List<Transform>();

        public LayerMask ignoreLayers;

        Ray GenerateMouseRay()
        {
            Vector3 mousePosFar = new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.farClipPlane);
            Vector3 mousePosNear = new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.nearClipPlane);

            Vector3 mousePosF = Camera.main.ScreenToWorldPoint(mousePosFar);
            Vector3 mousePosN = Camera.main.ScreenToWorldPoint(mousePosNear);

            Ray mr = new Ray(mousePosN, mousePosF - mousePosN);
            return mr;
        }

        public void InitGame()
        {
            initGame = true;
            ignoreLayers = (1 << 9);
        }

        private void Update()
        {
            if (!initGame) { return; }

            DragObjects();
            points.text = point.ToString();
        }

        void DragObjects()
        {
            if (Input.GetMouseButtonDown(0))
            {
                Ray mouseRay = GenerateMouseRay();
                RaycastHit hit;

                Debug.DrawRay(mouseRay.origin, mouseRay.direction, Color.yellow);
                if (Physics.Raycast(mouseRay.origin, mouseRay.direction, out hit, ignoreLayers))
                {
                    gObj = hit.transform.gameObject;
                    anchorHolder = hit.transform.parent;
                    objPlane = new Plane(Camera.main.transform.forward * -1, gObj.transform.position);

                    //calculadon o offset
                    Ray mRay = Camera.main.ScreenPointToRay(Input.mousePosition);
                    float rayDistance;
                    objPlane.Raycast(mRay, out rayDistance);
                    m0 = gObj.transform.position - mRay.GetPoint(rayDistance);
                }
            }
            else if (Input.GetMouseButton(0) && gObj)
            {
                Ray mRay = Camera.main.ScreenPointToRay(Input.mousePosition);
                float rayDistance;

                if (objPlane.Raycast(mRay, out rayDistance))
                {
                    Vector3 move = mRay.GetPoint(rayDistance) + m0;
                    gObj.transform.position = move;
                }
            }
            else if (Input.GetMouseButtonUp(0) && gObj)
            {
                RealeseObject();
            }
        }

        //Solta o objeto
        public void RealeseObject()
        {
            if (placeOn)
            {
                td.RighPlace(PlaceHolder);
                placeOn = false;
            }
            else
            {
                td.WorngPlace(PlaceHolder);
            }

            gObj = null;
        }

        public void ResetGame()
        {
            initGame = false;
            for (int i = 0; i < PlaceHolders.Count; i++)
            {
                Objs[i].transform.SetParent(PlaceHolders[i].transform);
                Objs[i].transform.position = PlaceHolders[i].transform.position;
                Objs[i].transform.rotation = PlaceHolders[i].transform.rotation;
                points.text = "0";
                point = 0;
            }
        }
    }
}
