﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Frameworks.RPG_TPC.Sourcecode
{
    public class LookAtCameraAlway : MonoBehaviour
    {
        public Transform camTrans;

        private void Update()
        {
            //LookAtCamera();
        }

        void LookAtCamera()
        {
            if (camTrans != null)
            {
                transform.LookAt(camTrans);
            }
        }
    }
}
