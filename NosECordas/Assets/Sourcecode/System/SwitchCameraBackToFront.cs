﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Vuforia;

namespace SoureCode.System
{
    public class SwitchCameraBackToFront : MonoBehaviour
    {
        private bool on = true;

        public void Switch()
        {
            on = !on;
            if (on)
            {
                CameraDevice.Instance.Stop();
                CameraDevice.Instance.Deinit();
                TrackerManager.Instance.GetTracker<ObjectTracker>().Stop();

#pragma warning disable CS0618 // Type or member is obsolete
                CameraDevice.Instance.Init(CameraDevice.CameraDirection.CAMERA_BACK);
#pragma warning restore CS0618 // Type or member is obsolete
                CameraDevice.Instance.Start();
                TrackerManager.Instance.GetTracker<ObjectTracker>().Start();

            }
            else if (!on)
            {
                CameraDevice.Instance.Stop();
                CameraDevice.Instance.Deinit();

                TrackerManager.Instance.GetTracker<ObjectTracker>().Stop();
#pragma warning disable CS0618 // Type or member is obsolete
                CameraDevice.Instance.Init(CameraDevice.CameraDirection.CAMERA_FRONT);
#pragma warning restore CS0618 // Type or member is obsolete

                CameraDevice.Instance.Start();
                TrackerManager.Instance.GetTracker<ObjectTracker>().Start();
            }
        }
    }
}
