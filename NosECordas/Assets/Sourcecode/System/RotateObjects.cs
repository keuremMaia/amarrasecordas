﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SoureCode.System
{
    public class RotateObjects : MonoBehaviour
    {
        /*[HideInInspector]*/
        public Transform root;
        public float rotateSpeed = 50f;
        public bool rotHumano = false;

        public bool rotLeft = false;
        public bool rotRight = false;
        public bool rotUp = false;
        public bool rotDown = false;

        #region Rotate Object
        private void Update()
        {
            RotateObject();
        }

        public void RotateObject()
        {
            if (rotLeft)
            {
                rotRight = false;
                if (rotHumano)
                {
                    root.Rotate(0, 0, rotateSpeed * Time.deltaTime);
                }
                else
                {
                    root.Rotate(0, rotateSpeed * Time.deltaTime, 0);
                }
            }

            if (rotRight)
            {
                rotLeft = false;
                if (rotHumano)
                {
                    root.Rotate(0, 0, (rotateSpeed * Time.deltaTime) * -1);
                }
                else
                {
                    root.Rotate(0, (rotateSpeed * Time.deltaTime) * -1, 0);
                }
            }

            if (rotUp)
            {
                //rotUp = false;
                root.Rotate(rotateSpeed * Time.deltaTime, 0, 0);
            }

            if (rotDown)
            {
                //rotDown = false;
                root.Rotate((rotateSpeed * Time.deltaTime) * -1, 0, 0);
            }
        }

        public void RotateLeftTrue()
        {
            rotLeft = true;
        }
        public void RotateLeftFalse()
        {
            rotLeft = false;
        }

        public void RotateRightTrue()
        {
            rotRight = true;
        }
        public void RotateRightFalse()
        {
            rotRight = false;
        }

        public void RotateUpTrue()
        {
            rotUp = true;
        }
        public void RotateUpFalse()
        {
            rotUp = false;
        }

        public void RotateDownTrue()
        {
            rotDown = true;
        }
        public void RotateDonwFalse()
        {
            rotDown = false;
        }
        #endregion

        public void ResetAll()
        {
            if (root != null)
            {
                root.rotation = new Quaternion(0, 0, 0, 0);
                //root.localScale = Vector3.one;
            }
        }

        public void GetTheRoot(Transform trt)
        {
            root = trt;
        }

        public void SetRootAsHuman(bool value)
        {
            rotHumano = value;
        }
    }
}
