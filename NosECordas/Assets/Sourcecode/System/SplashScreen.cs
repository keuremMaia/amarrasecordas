﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace SoureCode.System
{
    public class SplashScreen : MonoBehaviour
    {
        public int timer;
        public GameObject loading;

        void Start()
        {
            Invoke("GoToLoadingScreen", timer);
        }

        void GoToLoadingScreen()
        {
            loading.SetActive(true);
        }
    }
}
