﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace SoureCode.System
{
    public class FakeLoading : MonoBehaviour
    {
        public Image loadingBar;
        public GameObject Icon;
        public GameObject Next;
        public GameObject iconNext;
        private float cnt = 0;

        private void Start()
        {
            if (loadingBar.fillAmount == 1) { loadingBar.fillAmount = 0; }
        }

        private void Update()
        {
            cnt += Time.deltaTime;
            if (cnt <= 1) { FakeLoadingStart(); }

            if (cnt >= 1)
            {
                Next.SetActive(true);
                //iconNext.SetActive(true);
                //Icon.SetActive(false);
            }
        }

        void FakeLoadingStart()
        {
            loadingBar.fillAmount = cnt;
        }
    }
}

