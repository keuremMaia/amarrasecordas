﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace SoureCode.System
{
    public class TriggerDetection : MonoBehaviour
    {
        public TriggerDetection td;
        public PiramideGame pg;
        public BoxCollider cb;
        public GameObject Placas;

        private void Start()
        {
            td = GetComponent<TriggerDetection>();
            cb = GetComponent<BoxCollider>();
        }

        private void OnTriggerStay(Collider other)
        {
            pg.PlaceHolder = other;
            pg.td = td;

            if (other.gameObject.name == this.gameObject.name)
            {
                pg.placeOn = true;
            }
            else if (other.gameObject.name != this.gameObject.name)
            {
                pg.placeOn = false;
            }
        }

        public void RighPlace(Collider other)
        {
            other.transform.SetParent(this.transform);
            other.transform.position = transform.position;
            other.transform.rotation = transform.rotation;
            other.GetComponent<Collider>().enabled = false;
            Placas.SetActive(true);
            pg.point += 10;
            cb.enabled = false;
            td.enabled = false;
        }

        public void WorngPlace(Collider other)
        {
            other.gameObject.transform.position = pg.anchorHolder.transform.position;
            other.gameObject.transform.rotation = pg.anchorHolder.transform.rotation;
        }
    }
}
