﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace SoureCode.System
{
    public class SystemManager : MonoBehaviour
    {
        [Header("Managers")]
        public UIManager uIManager;
        public GameObject ButtonsHodler;

        public bool lockObject;
        public CardTypeDetected typeDetected;
        public float rotateSpeed = 50f;

        /*[HideInInspector]*/ public Transform plataform;
        /*[HideInInspector]*/ public Transform plataformRoot;
        /*[HideInInspector]*/ public Transform plataformLockOn;
        [HideInInspector] public OnTargetFoundVuforia vufo;

        public bool rotLeft = false;
        public bool rotRight = false;
        public bool rotUp = false;
        public bool rotDown = false;

        [Header("Setas")]
        public Animator c_anim;
        public string[] c_targetAnimsNext;
        public string[] c_targetAnimsPrevius;
        public int animIndex = 0;
        public int c_tagergetIndex;

        #region Singleton
        public static SystemManager instace;

        private void Awake()
        {
            instace = this;
        }
        #endregion

        #region Rotate Object
        public void RotateObject()
        {
            if (rotLeft)
            {
                rotRight = false;
                plataform.Rotate(0, rotateSpeed * Time.deltaTime, 0);
                plataformLockOn.Rotate(0, rotateSpeed * Time.deltaTime, 0);
            }

            if (rotRight)
            {
                rotLeft = false;
                plataform.Rotate(0, (rotateSpeed * Time.deltaTime) * -1, 0);
                plataformLockOn.Rotate(0, (rotateSpeed * Time.deltaTime) * -1, 0);
            }

            if (rotUp)
            {
                //rotUp = false;
                plataform.Rotate(rotateSpeed * Time.deltaTime, 0, 0);
                plataformLockOn.Rotate(rotateSpeed * Time.deltaTime, 0, 0);
            }

            if (rotDown)
            {
                //rotDown = false;
                plataform.Rotate((rotateSpeed * Time.deltaTime) * -1, 0, 0);
                plataformLockOn.Rotate((rotateSpeed * Time.deltaTime) * -1, 0, 0);
            }
        }

        public void RotateLeftDown()
        {
            rotLeft = true;
        }
        public void RotateLeftUp()
        {
            rotLeft = false;
        }

        public void RotateRightDown()
        {
            rotRight = true;
        }
        public void RotateRightUp()
        {
            rotRight = false;
        }

        public void RotateUpDown()
        {
            rotUp = true;
        }
        public void RotateUpUp()
        {
            rotUp = false;
        }

        public void RotateDownDown()
        {
            rotDown = true;
        }
        public void RotateDonwUp()
        {
            rotDown = false;
        }
        #endregion

        private void Start()
        {
            uIManager.Init(this);
        }

        private void Update()
        {
            uIManager.UITick();
        }

        public void ResetAll()
        {
            if (plataformLockOn != null && plataform != null)
            {
                plataform.rotation = new Quaternion(0, 0, 0, 0);
                plataformLockOn.rotation = new Quaternion(0, 0, 0, 0);
                plataform.localScale = Vector3.one;
                plataformLockOn.localScale = Vector3.one;
            }
        }

        public void LockOnExecution()
        {
            //if (plataformLockOn == null || plataform == null) { return; }

            //lockObject = true;

            //if (lockObject)
            //{
            //    plataformLockOn.position = plataform.position;
            //    plataformLockOn.eulerAngles = plataform.localEulerAngles;

            //    uIManager.lockOnObj.image.sprite = uIManager.LockOnImage;

            //    plataform.SetParent(plataformLockOn);

            //    plataform.position = plataformLockOn.position;
            //    plataform.rotation = plataformLockOn.rotation;

            //    ButtonsHodler.SetActive(true);

            //    uIManager.lockOnObj.onClick.RemoveAllListeners();
            //    uIManager.lockOnObj.onClick.AddListener(delegate { UnLock(); });
            //}
        }

        public void ChangeAlphaFromMaterial(Material m)
        {
            Color c = m.color;
            c.a = 0.3f;
            m.color = c;
        }

        public void ChangeAlphaBackFromMaterial(Material m)
        {
            Color c = m.color;
            c.a = 1;
            m.color = c;
        }
        
        public void NextComparation()
        {
            if (animIndex == c_tagergetIndex) { return; }

            c_anim.Play(c_targetAnimsNext[animIndex]);
            animIndex++;
            if (animIndex > c_tagergetIndex)
            {
                animIndex = c_tagergetIndex;
            }
        }

        public void PreviusComparation()
        {
            animIndex--;
            if (animIndex < 0)
            {
                animIndex = 0;
            }
            c_anim.Play(c_targetAnimsPrevius[animIndex]);
        }

        public void UnLock()
        {
            //if (plataformLockOn == null || plataform == null) { return; }

            //lockObject = false;

            //if (!lockObject)
            //{
            //    uIManager.lockOnObj.image.sprite = uIManager.LockOffImage;

            //    plataform.SetParent(plataformRoot);
            //    plataform.position = plataformRoot.position;
            //    plataform.rotation = plataformRoot.rotation;
            //    //rotHumano = false;

            //    ButtonsHodler.SetActive(false);

            //    //vufo.OnTrackingLost();
            //    uIManager.lockOnObj.onClick.RemoveAllListeners();
            //    uIManager.lockOnObj.onClick.AddListener(delegate { LockOnExecution(); });
            //}
        }

        public void ExitButton()
        {
            Application.Quit();
        }
    }

    public enum CardTypeDetected
    {
        None,
        Alive,
        Comparacoes
    }
}
