﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Lean;

public class ImageSwapper : MonoBehaviour
{
    public List<GameObject> images = new List<GameObject>();
    public int index = 0;
    public GameObject prevButton;
    public GameObject nextButton;
    public GameObject PanelDicas;
    [SerializeField] bool useButtons = false;

    bool useSwipeRight = false;
    bool useSwipeLeft = true;
    public void OnEnable()
    {
        if (images[index].GetComponent<Animator>() != null)
        {
            images[index].GetComponent<Animator>().Play("SlideOutTop");
        }


        //if (prevButton != null || nextButton != null)
        //{
        //    prevButton.SetActive(false);
        //    nextButton.SetActive(true);
        //}

        SetButtons();

        if (images[index].GetComponent<Animator>() != null)
        {
            images[index].GetComponent<Animator>().Play("SlideIn");
        }


    }

    public void SetIndex(int i)
    {
        index = i;
    }

    public void DelayedShutDown()
    {
        if (images[index].GetComponent<Animator>() != null)
        {
            images[index].GetComponent<Animator>().Play("SlideOutTop", 0, 9f);

        }
        Invoke("SetGameObjectActive", 0.1f);
    }

    public void SetGameObjectActive()
    {

        PanelDicas.SetActive(false);
    }

    public virtual void SetButtons()
    {
        if (index == 0)
        {
            if (useButtons)
            {
                prevButton.SetActive(false);
                nextButton.SetActive(true);
            }
            else
            {
                useSwipeLeft = true;
                useSwipeRight = false;
            }
        }
        // Fim
        else if (index >= images.Count - 1)
        {
            if (useButtons) { prevButton.SetActive(true); nextButton.SetActive(false); }
            else { useSwipeLeft = false; useSwipeRight = true; }
        }
        // meio
        else
        {
            // Debug.Log("esta Entrando aqui");
            if (useButtons) { nextButton.SetActive(true); prevButton.SetActive(true); }
            else { useSwipeLeft = true; useSwipeRight = true; }
        }
    }

    void DicasController(bool active)
    {
        if (images[index].GetComponent<Animator>() != null) { images[index].GetComponent<Animator>().Play("SlideOutTop"); }
        else { images[index].SetActive(active); }
    }

    public void LeftSwipe()
    {
        if (useSwipeLeft == true)
        {
            NextClick();
        }
    }
    public virtual void PrevClick()
    {
        if (images[index].GetComponent<Animator>() != null) { MakeAnimation("SlideOutTop", "BackClick"); }
        else { DicasController(false); Invoke("BackClick", 0.0f); }
        SetButtons();
    }
    public void BackClick()
    {
        if (index > 0)
        {
            index--;
            if (images[index].GetComponent<Animator>() != null) { images[index].GetComponent<Animator>().Play("SlideInTop"); }
            else { DicasController(true); }
            SetButtons();
        }
    }
    void MakeAnimation(string animationName, string invokeName)
    {
        images[index].GetComponent<Animator>().Play(animationName);
        Invoke(invokeName, 0.0f);
    }

    public void RightSwipe()
    {
        if (useSwipeRight == true)
        {
            PrevClick();
        }
    }

    public virtual void NextClick()
    {
        if (images[index].GetComponent<Animator>() != null)
        {

            MakeAnimation("SlideOut", "DelayNExt");

        }
        else { DicasController(false); Invoke("DelayNExt", 0.0f); }
        SetButtons();
    }
    public void DelayNExt()
    {
        if (index < images.Count)
        {
            index++;
            if (images[index].GetComponent<Animator>() != null) { images[index].GetComponent<Animator>().Play("SlideIn"); }
            else { DicasController(true); }
            SetButtons();
        }
    }

}