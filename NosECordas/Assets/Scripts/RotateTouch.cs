﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Touch;

public class RotateTouch : MonoBehaviour
{
    public float rotate;
    [Tooltip("Ignore fingers with StartedOverGui?")]
    public bool IgnoreStartedOverGui = true;
    [Tooltip("Ignore fingers with IsOverGui?")]
    public bool IgnoreIsOverGui;
    [Tooltip("Ignore fingers if the finger count doesn't match? (0 = any)")]
    public int RequiredFingerCount;
    [Tooltip("Does translation require an object to be selected?")]
    public LeanSelectable RequiredSelectable;
    [Tooltip("The camera the translation will be calculated using (None = MainCamera)")]
    public Camera Camera;
    public Vector3 lastPointClicked;

#if UNITY_EDITOR
    protected virtual void Reset()
    {
        Start();
    }
#endif
    protected virtual void Start()
    {
        if (RequiredSelectable == null)
        {
            RequiredSelectable = GetComponent<LeanSelectable>();
        }
    }

    protected virtual void Update()
    {
        var fingers = LeanSelectable.GetFingers(IgnoreStartedOverGui, IgnoreIsOverGui, RequiredFingerCount, RequiredSelectable);

        var screenDelta = LeanGesture.GetScreenDelta(fingers);
        if (screenDelta != Vector2.zero)
        {
            Translate(screenDelta);
        }
    }

    protected virtual void Translate(Vector2 screenDelta)
    {
        if (screenDelta.x < 0)
        {
            transform.Rotate(0, rotate, 0, Space.Self);
        }
        else
        {
            transform.Rotate(0, -rotate, 0, Space.Self);
        }
    }
}
