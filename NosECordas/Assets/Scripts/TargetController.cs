﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetController : MonoBehaviour
{

    [SerializeField] Animator Animator;


    // Use this for initialization
    void OnEnable()
    {
        Animation animation = Animator.GetComponent<Animation>();
        Animator.Play("Play", 0, 0.999999f);
        StopAnimation();
    }

    [ContextMenu("Stop Animation")]
    public void StopAnimation()
    {
        Animator.speed = 0;
    }

    [ContextMenu("Continue animation")]
    public void PlayAnimation()
    {
        Animator.speed = 1;

    }
}