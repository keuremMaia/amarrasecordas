﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioMute : MonoBehaviour
{

    void Start()
    {
        // iPhoneSpeaker.ForceToSpeaker();
    }

    // Update is called once per frame
    void Update()
    {

    }


    public void MuteSound()
    {
        AudioListener.volume = 0;
    }

    public void UnMuteSound()
    {
        print("unmuting");
        AudioListener.volume = 1;
    }

}
