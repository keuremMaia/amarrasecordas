﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "LanguageManager", menuName = "ScriptableObjects/LanguageManager", order = 1)]
public class LanguageManager : ScriptableObject
{
    string Language;

    public void SetLanguage(string lang)
    {
        Language = lang;
    }

    public string GetLanguage()
    {
        return Language;
    }

}
