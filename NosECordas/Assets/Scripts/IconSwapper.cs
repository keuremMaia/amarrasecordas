﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IconSwapper : MonoBehaviour
{
    public ImageListScriptable iconList;
    public List<GameObject> PioneriaList = new List<GameObject>();

    protected int index = 0;
    public GameObject prevButton;
    public GameObject nextButton;
    public Image image;
    public Animator animator;
    bool firstPlay = true;
    protected virtual void OnEnable()
    {
        SetIndex(0);

        NextFBX();
    }


    public void OnDisable()
    {
        PioneriaList[index].SetActive(false);
    }

    protected void SetIndex(int i)
    {
        index = i;
    }

    public void NextImage()
    {
        image.sprite = iconList.GetImage(index);
    }

    public virtual void PrevClick()
    {
        PioneriaList[index].SetActive(false);
        index--;
        NextFBX();
    }

    public virtual void NextFBX()
    {
        PioneriaList[index].SetActive(true);
        NextImage();
        if (firstPlay)
        {
            firstPlay = false;
        }
        else
        {
            animator.Play("PioneriaNext");
        }
    }

    public virtual void NextClick()
    {
        PioneriaList[index].SetActive(false);
        index++;
        NextFBX();
    }
}
