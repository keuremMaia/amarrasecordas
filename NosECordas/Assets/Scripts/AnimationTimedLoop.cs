﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationTimedLoop : MonoBehaviour
{

    [SerializeField] Animator animator;
    [SerializeField] float LoopTime;
    [SerializeField] string animationName;

    // Use this for initialization
    void OnEnable()
    {
        Debug.Log("Entrou");
        StartCoroutine(AnimationLoop());
    }

    // Update is called once per frame
    void Update()
    {

    }


    IEnumerator AnimationLoop()
    {
        while (true)
        {
            yield return new WaitForSeconds(LoopTime);
            Debug.Log("Teste na animacao");
            animator.Play(animationName, 0, 0);
            // animator.Play(animationName);



        }

    }
}
