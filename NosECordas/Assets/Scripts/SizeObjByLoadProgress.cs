﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SizeObjByLoadProgress : MonoBehaviour
{

    public Image meshRenderer;
    // Use this for initialization
    void Start()
    {
        // meshRenderer = GetComponent<MeshRenderer>();
    }

    // Update is called once per frame
    void Update()
    {

        float value = LoadAsync.progress;
        print(LoadAsync.progress);
        meshRenderer.fillAmount = value;
        //meshRenderer.material.SetFloat("_Value", value);
    }
}
