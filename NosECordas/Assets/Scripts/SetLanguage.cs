﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetLanguage : MonoBehaviour
{
    public Lean.Localization.LeanLocalization location;
    public LanguageManager languageManager;
    // Use this for initialization
    void Start()
    {
        location.SetCurrentLanguage(languageManager.GetLanguage());
    }
}
