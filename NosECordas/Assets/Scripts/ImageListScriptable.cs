﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "ImageHolder", menuName = "ScriptableObjects/ImageHolder", order = 1)]
public class ImageListScriptable : ScriptableObject
{

    public List<Sprite> images = new List<Sprite>();


    public Sprite GetImage(int i)
    {
        return images[i];
    }

}
