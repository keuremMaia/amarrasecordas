﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayPause : MonoBehaviour
{
    [SerializeField]
    CurrentActivesCards activesCards;
    Animator anim;
    string targetAnim;
    public GameObject PlayButton;
    public GameObject PauseButton;
    bool continueAnimation;
    private void OnDisable()
    {
        continueAnimation = false;
        PlayButton.SetActive(true);
        PauseButton.SetActive(false);
    }
    private void OnEnable()
    {
        continueAnimation = false;
    }
    public void GetAnimator(Animator animator)
    {
        if (animator != null)
        {
            anim = animator;
        }
    }
    public void PlayAnimation(string Play)
    {
        if (continueAnimation == false)
        {
            GetTargetAnim(Play);
            PlayAnimButton();
            continueAnimation = true;
        }
        else
        {
            anim.speed = 1;
        }
        PlayButton.SetActive(false);
        PauseButton.SetActive(true);
    }
    void GetTargetAnim(string targetAnim)
    {
        this.targetAnim = targetAnim;
    }
    void PlayAnimButton()
    {
        if (anim != null)
        {
            anim.Play(targetAnim, 0, 0.00000f);
            anim.speed = 1;
        }
    }
    public void StopAnim()
    {
        if (continueAnimation == true)
        {
            PlayButton.SetActive(true);
            anim.speed = 0;
        }
    }
    private void Update()
    {
        if (anim != null)
        {
            AnimatorStateInfo animationState = anim.GetCurrentAnimatorStateInfo(0);
            float myTime = animationState.normalizedTime;
            if (myTime > 0.99999999999)
            {
                continueAnimation = false;
                PlayButton.SetActive(true);
                PauseButton.SetActive(false);
            }
        }
    }
}
