﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadAsync : MonoBehaviour {

    public static float progress;

    private void Start()
    {
        LoadScene("Main");
    }

    public void LoadScene(string name)
    {
        StartCoroutine(LoadOparation(name));
    }

    IEnumerator LoadOparation(string name)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(name);

        while (!operation.isDone)
        {
            progress = operation.progress;
            ///print(progress);
            yield return new WaitForSeconds(0.1f);
        }
    }
}
