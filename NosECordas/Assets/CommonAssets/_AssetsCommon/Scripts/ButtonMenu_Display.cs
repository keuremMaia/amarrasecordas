﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonMenu_Display : MonoBehaviour {

    [SerializeField] RectTransform SelfRect;
    bool bPosicaoInicial = true;
    [SerializeField] float PosicaoFinal;
    float PosicaoInicial;
    //float largura = 111;

    private void Start()
    {
        PosicaoInicial = SelfRect.position.x; 
    }

    public void OnClick()
    {
        if (bPosicaoInicial == true)
        {
            bPosicaoInicial = false;
            SelfRect.anchoredPosition = new Vector2(PosicaoFinal, SelfRect.anchoredPosition.y);
        }
        else
        {
            bPosicaoInicial = true;
            SelfRect.anchoredPosition = new Vector2(PosicaoInicial, SelfRect.anchoredPosition.y);
        }
    }
}
