﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;
public class RotateAround : MonoBehaviour
{

    public bool swiping;

    public float minSwipeDistance;
    public float errorRange;

    public SwipeDirection direction = SwipeDirection.None;

    public enum SwipeDirection { Right, Left, Up, Down, None }

    private Touch initialTouch;

    void Start()
    {
        Input.multiTouchEnabled = true;
    }

  

    public void OnTap()
    {
        Debug.Log("AAAAAAAAAAAAAA");
    }



    void CalculateSwipeDirection(float deltaX, float deltaY)
    {
        bool isHorizontalSwipe = Mathf.Abs(deltaX) > Mathf.Abs(deltaY);

        // horizontal swipe
        if (isHorizontalSwipe && Mathf.Abs(deltaY) <= errorRange)
        {
            //right
            if (deltaX > 0)
                direction = SwipeDirection.Right;
            //left
            else if (deltaX < 0)
                direction = SwipeDirection.Left;
        }
        //vertical swipe
        else if (!isHorizontalSwipe && Mathf.Abs(deltaX) <= errorRange)
        {
            //up
            if (deltaY > 0)
                direction = SwipeDirection.Up;
            //down
            else if (deltaY < 0)
                direction = SwipeDirection.Down;
        }
        //diagonal swipe
        else
        {
            swiping = false;
        }


    }
}
