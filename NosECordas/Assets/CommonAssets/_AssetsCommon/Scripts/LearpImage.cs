﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LearpImage : MonoBehaviour {

    public float visibleTime;
    public float fadeTime;

    private Image image;
    private float timer = 0;

    private bool canStart = false;
	// Use this for initialization
	void Start () {
        image = GetComponent<Image>();
        StartCoroutine(Wait());
	}

    IEnumerator Wait()
    {
        yield return new WaitForSeconds(visibleTime);
        canStart = true;
    }

    private void Update()
    {
        if (!canStart)
            return;

        if (timer< fadeTime)
        {
            var cor = image.color;
            cor.a =  Mathf.Lerp(1, 0, timer/fadeTime);
            image.color = cor;
            timer += Time.deltaTime;
        }
        else
        {
            gameObject.SetActive(false);
            canStart = false;
        }
    }
}
