﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetSubMenuInfo : MonoBehaviour {

    [SerializeField] SubMenuButtonValues infosValue;

    private void Start()
    {
        Set();
    }

    [ContextMenu("Set")]
    public void Set()
    {
        Image[] imageArray = GetComponentsInChildren<Image>();
        //pulou o 0 pois é o componente da imagem de fundo.
        imageArray[1].sprite = infosValue.numberImage;
        imageArray[2].sprite = infosValue.desenhoImage;

        GetComponentInChildren<Text>().text = infosValue.text;
    }
}
