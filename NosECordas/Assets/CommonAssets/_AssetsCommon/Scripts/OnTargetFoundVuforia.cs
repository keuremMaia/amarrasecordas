﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Vuforia;

public class OnTargetFoundVuforia : MonoBehaviour, ITrackableEventHandler
{
    [SerializeField]
    UnityEvent onFoundEventList;
    [SerializeField]
    public UnityEvent onLostObjEventList;

    //usado para o cadeado, para nao desativar o obj quando perder o track
    static public bool stopTrackEvent = false;
    TrackableBehaviour.Status currentStatus;

    private TrackableBehaviour mTrackableBehaviour;

    void Start()
    {
        mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (mTrackableBehaviour)
        {
            mTrackableBehaviour.RegisterTrackableEventHandler(this);

        }
    }

    public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
    {
        currentStatus = newStatus;
        if (stopTrackEvent)
            return;

        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {

            onFoundEventList.Invoke();
        }
        else
        {
            onLostObjEventList.Invoke();
        }
    }

    public void ActiveObjForEditing()
    {
        GetComponentsInChildren<Transform>(true)[1].gameObject.SetActive(true);
    }

    public void DeactiveObjAfeterEditing()
    {
        GetComponentsInChildren<Transform>()[1].gameObject.SetActive(false);
    }

    public void OnExitUnlock()
    {
        if (currentStatus == TrackableBehaviour.Status.TRACKED)
        {
            return;
        }
        else
        {
            onLostObjEventList.Invoke();
        }
    }

    [ContextMenu("Ativar")]
    public void ActiveByEditor()
    {
        onFoundEventList.Invoke();
    }

    [ContextMenu("Desativar")]
    public void DeactiveByEditor()
    {
        onLostObjEventList.Invoke();
    }
}

