﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "buttonSubMenu", menuName = "vinicius/SubMenu", order = 0)]
public class SubMenuButtonValues : ScriptableObject
{
    public Sprite numberImage;
    public Sprite desenhoImage;
    public string text;
}
