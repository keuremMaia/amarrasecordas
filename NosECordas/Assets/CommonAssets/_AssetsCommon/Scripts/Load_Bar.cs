﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Load_Bar : MonoBehaviour
{

    //MeshRenderer meshRenderer;
    Slider loadBar;
    // Use this for initialization
    void Start()
    {
        loadBar = GetComponent<Slider>();
        StartLoad();
    }

    public void StartLoad()
    {
        StartCoroutine(LoadYourAsyncScene());
    }

    IEnumerator LoadYourAsyncScene()
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("Main");
        while (!asyncLoad.isDone)
        {
            loadBar.value = asyncLoad.progress;
            yield return null;
        }
    }

}
