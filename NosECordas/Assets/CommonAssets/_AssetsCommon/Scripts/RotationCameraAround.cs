﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ControlFreak2;

public class RotationCameraAround : MonoBehaviour
{
    [HideInInspector]//must be set by script
    public Transform rotationTarget;

    [SerializeField]
    readonly float speed = 1;

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 1)
            return;

        float h = CF2Input.GetAxis("Horizontal");
        float v = CF2Input.GetAxis("Vertical");

        transform.RotateAround(rotationTarget.transform.position, new Vector2(-v, h), speed);
    }
}

