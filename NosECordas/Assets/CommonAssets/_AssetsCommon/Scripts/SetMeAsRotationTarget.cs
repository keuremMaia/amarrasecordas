﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetMeAsRotationTarget : MonoBehaviour {

    private void OnEnable()
    {
        RotationCameraAround subCam = Camera.main.GetComponentInChildren<RotationCameraAround>();
        subCam.transform.localEulerAngles = Vector3.zero;
        subCam.transform.localPosition = Vector3.zero;
        subCam.rotationTarget = this.transform;
    }
}
